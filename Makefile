.PHONY: up down test

up:
	vagrant up postgres gitlab

down:
	vagrant destroy -f

test-ce:
	./test.sh | tee ce_qa_out.txt

test-ee:
	./test.sh ee | tee ee_qa_out.txt

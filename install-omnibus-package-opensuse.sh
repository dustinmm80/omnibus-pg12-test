#!/bin/bash -e

INSTANCE_HOSTNAME="$(hostname -I | cut -d' ' -f2).xip.io"
PACKAGE_NAME='gitlab.rpm'

echo 'Installing omnibus-gitlab!'

# Copy gitlab config
mkdir -p /etc/gitlab
cp /vagrant/gitlab.rb /etc/gitlab/gitlab.rb
chmod 600 /etc/gitlab/gitlab.rb

zypper update
zypper install -y curl openssh ca-certificates

export EXTERNAL_URL="http://$INSTANCE_HOSTNAME"

zypper --no-gpg-checks install -y "/vagrant/pkg/$PACKAGE_NAME"

#!/bin/bash -e

PACKAGE_NAME="${PACKAGE_NAME:-gitlab.rpm}"
INSTANCE_HOSTNAME="$(hostname -I | cut -d' ' -f2).xip.io"

echo 'Installing omnibus-gitlab!'

# Copy gitlab config
mkdir -p /etc/gitlab
cp /vagrant/gitlab.rb /etc/gitlab/gitlab.rb
chmod 600 /etc/gitlab/gitlab.rb

yum install -y curl policycoreutils-python openssh-server perl

export EXTERNAL_URL="http://$INSTANCE_HOSTNAME"

yum install -y "/vagrant/pkg/$PACKAGE_NAME"

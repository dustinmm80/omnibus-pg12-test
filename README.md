# omnibus-pg12-test

Vagrant setup to test Postgres 12 with omnibus gitlab.
This is for https://gitlab.com/gitlab-org/omnibus-gitlab/-/issues/5102.

|Software|Version|
|-|-|
|GitLab|12.9.2-ee.0|
|PostgreSQL|12.2-2.pgdg18.04+1|
|Ubuntu|18.04.4 LTS|

Root password is "lemonade". 

```
vagrant up postgres gitlab

open http://localhost:8000
```

Use `vagrant destroy -f` to remove VMs after testing.

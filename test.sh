#!/bin/bash -e

INSTANCE_TYPE="${1:-ce}"

INSTANCE_URL='http://192.168.56.2'
GITLAB_VERSION='13.1.0'

if [ "$INSTANCE_TYPE" == "ce" ]; then
  TEST_SUITE="CE:$GITLAB_VERSION"
else
  TEST_SUITE="EE:$GITLAB_VERSION-ee"
fi

export GITLAB_USERNAME=root
export GITLAB_ADMIN_USERNAME=root
export GITLAB_PASSWORD=lemonade
export GITLAB_ADMIN_PASSWORD=lemonade
export EE_LICENSE="$(cat GitLab.gitlab-license)"
# export QA_DEBUG=true

bundle exec gitlab-qa \
  Test::Instance::Any "$TEST_SUITE" "$INSTANCE_URL"

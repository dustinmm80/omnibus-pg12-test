#!/bin/bash -e

PACKAGE_NAME="${PACKAGE_NAME:-gitlab.deb}"
INSTANCE_HOSTNAME="$(hostname -I | cut -d' ' -f2).xip.io"

echo 'Installing omnibus-gitlab!'

# Copy gitlab config
mkdir -p /etc/gitlab
cp /vagrant/gitlab.rb /etc/gitlab/gitlab.rb
chmod 600 /etc/gitlab/gitlab.rb

apt-get update
apt-get install -y curl openssh-server ca-certificates

export EXTERNAL_URL="http://$INSTANCE_HOSTNAME"

apt-get install -y "/vagrant/pkg/$PACKAGE_NAME"
